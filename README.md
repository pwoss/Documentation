# We moved everything to our own Gitea instance -> [git.pwoss.xyz](https://git.pwoss.xyz/PwOSS-Server/Documentation)

# Server - Software

&nbsp;

## Arch Linux 64-bit / Arch Linux ARM (Raspberry image)

### [Raspberry Pi](https://github.com/PwOSS/Documentation#raspberry-pi---image)
The Raspberry Pi is the most popular single-board computer.

It's perfectly made (among other things) to get a running server at your home. It does not cost much power and it's small so it fits everywhere. Your power bill will be less than $30 a year (for the server).

The image (operating system) what we provide is for the Pi 2,3 and 3B +.

> Other ARM devices:  
> It would be perfect if you could help with other ARM devices. The scratch file should work on all ARM devices (ArchLinux | ARM - Images).

&nbsp;

### [Arch Linux 64-bit](https://github.com/PwOSS/Documentation#arch-linux-64-bit---scratch-file)
A 64-bit server offers much more power than a Raspberry Pi. However, the cost of power (electricity) and hardware could be more than a Raspberry Pi. Depends on the system.

&nbsp;

## Download

### Raspberry - Image
The "PwOSS - Server" is currently only available on our website.  
You can use the [Raspberry Pi - Image](https://seafile.pwoss.xyz/d/1215a57671da473cadbe/?p=/1.%20Raspberry%20Pi/Arch%20Linux%20%7C%20ARM/PwOSS%20-%20Image/Latest&mode=list) (PW=PwOSS.xyz) and the .md file at [seafile.pwoss.xyz](https://seafile.pwoss.xyz/d/1215a57671da473cadbe/?p=/1.%20Raspberry%20Pi/Arch%20Linux%20%7C%20ARM/PwOSS%20-%20Image/Latest&mode=list) as well or at [GitHub](https://github.com/PwOSS/Documentation/blob/master/Raspberry/PwOSS%20-%20Server%20%7C%20Raspberry%20-%20Archlinux%20%7C%20ARM%20-%20(Image%20Docu).md) to get a running server at home.

### Raspberry - Scratch File
If you don't want to use our image you can start from scratch.  
Just download the Rasppberry Pi - Image from [archlinuxarm.org](https://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2#installation) and follow the [PwOSS - Server | Raspberry - Archlinux | ARM - (Scratch Docu).md](https://github.com/PwOSS/Documentation/blob/master/Raspberry/PwOSS%20-%20Server%20%7C%20Raspberry%20-%20Archlinux%20%7C%20ARM%20-%20(Scratch%20Docu).md) file.

### Raspberry - Guideline
You can use the [Guideline](https://guideline.pwoss.xyz/server/raspberry-pi).

&nbsp;

### Arch Linux (64-bit) - Scratch File
There is no image / iso for now. We might be create a bootable USB stick or / and a .vdi (VirtualBox) file. You have to use the guide & scratch combo for now.  
You'll find the scratch docu here [GitHub](https://github.com/PwOSS/Documentation/tree/master/Arch%2064-bit) or from our server [seafile.pwoss.xyz](https://seafile.pwoss.xyz/d/1215a57671da473cadbe/?p=/3.%20Arch%20Linux%20-%2064-bit/Arch%20Linux%20-%20%28Scratch%20Docu%29&mode=list).

### Arch Linux (64-bit) - Guideline
You can use the [Guideline](https://guideline.pwoss.xyz/server/arch%20linux).

&nbsp;

## Problems? You want to add/change something?
You can create an __issue__ or a __pull request__ here on Github or visit us in our forum at [https://forum.pwoss.xyz](https://forum.pwoss.xyz/).

&nbsp;

## Info
More information about __PwOSS - Privacy with Open Source Software__ at https://pwoss.xyz/.

&nbsp;
&nbsp;

## License
<a href="http://creativecommons.org/licenses/by-sa/4.0/" rel="license"><img style="border-width: 0;" src="https://pwoss.xyz/wp-content/uploads/2018/07/licensebutton.png" alt="Creative Commons License" /></a>
