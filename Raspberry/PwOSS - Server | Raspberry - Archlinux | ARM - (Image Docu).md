## PwOSS - Server | Raspberry - Archlinux | ARM
(Image Docu)

&nbsp;

### Important - before you start check following:

1. Your router needs the possibility of port forwarding and the possibility to configure the DNS server for Pi-hole.  
2. You’ll need a DynDNS-Domain. For example, at https://www.noip.com/sign-up.  
3. You have to connect a USB Stick/External HardDrive

&nbsp;

### Info / Tip
Some commands must be changed by you. The keywords will start with '__your-__'.
- your-interface
- your-password
- your-location
- etc.

We will mark it with the words '__Input required:__  ' above the commands.

Hit the tab key for autocompletion when typing commands.

&nbsp;

Flash the PwOSS - Image file on your SD-Card with Etcher or with the following terminal command.
```
sudo dd bs=4M if=/your/path/to/PwOSS-Server_Raspberry-ArchLinux-ARM-xx.xx.xxxx.img of=/dev/mmcblk0
sync
```
Make sure _/dev/mmcblk0_ is your SD Card!!!

&nbsp;

### For SSH without keyboard. Connection from PC/Smartphone per Terminal to Raspberry - Arch

You have to find your local IP address of your raspberry pi to connect per SSH to the terminal.

Terminal command:

```
arp -n | awk '/b8:27:eb/ {print $1}'
```

In my constellation, it's 192.168.1.76. You can find it on your router as well.

&nbsp;

Open your terminal and type:

```
ssh pwoss@192.168.1.76
pwoss
```

&nbsp;

## 1. PwOSS-Image (Resize SD CARD)
```
sudo fdisk /dev/mmcblk0
d
2
n
p
2
```
Enter

```
Y
w
sudo reboot now -h
```
```
ssh pwoss@192.168.1.76
pwoss
sudo resize2fs /dev/mmcblk0p2
```

&nbsp;

## 2. Change TimeZone

```
timedatectl list-timezones
```

Choose your timezone and copy it.  
ctrl z

&nbsp;

__Input required:__  
```
timedatectl set-timezone your-location
```
ctrl + x  
yes

&nbsp;

## 3.. dm-crypt LUKS
```
sudo cryptsetup -v luksFormat /dev/sda
```

YES  

&nbsp;

__Input required:__    
passphrase=your-password

```
sudo cryptsetup luksOpen /dev/sda externalHD
```

&nbsp;

__Input required:__    
passphrase=your-password

```
sudo mkfs.ext4 /dev/mapper/externalHD
sudo mkdir /mnt/externalHD
sudo cryptsetup luksClose externalHD
sudo dd if=/dev/urandom of=/home/pwoss/.key bs=4096 count=4
sudo chmod 400 /home/pwoss/.key
sudo cryptsetup luksAddKey /dev/sda /home/pwoss/.key
```

&nbsp;

__Input required:__    
passphrase=your-password

```
sudo nano /etc/crypttab
```

Add to the bottom:

```
externalHD /dev/sda /home/pwoss/.key luks
```
ctrl + x  
yes

```
sudo nano /etc/fstab
```

Add to the bottom:

```
/dev/mapper/externalHD /mnt/externalHD ext4 defaults 0 0
```
ctrl + x  
yes

Do a reboot.

```
sudo reboot now -h
```

&nbsp;

## 4. Radicale ( Calendar & Contact Server)
```
sudo nano /etc/radicale/config
```

Change the IP to yours:

```
hosts = 192.168.1.76:5232
```
ctrl + x  
yes

```
sudo nano /etc/radicale/users
```

&nbsp;

__Input required:__    
> Change to your Family Member for example.

```
your-user1:your-user1_password
your-user2:your-user2_password
...
```
ctrl + x  
yes

```
sudo systemctl restart radicale
```

> check http://your-server_ip:5232

&nbsp;

## 5. OpenVPN

&nbsp;

### Server-Config
```
sudo nano /etc/openvpn/server/server.conf
```

> Change the IP to your home network if it’s necessary.

```
# your local subnet
push "route 192.168.1.0 255.255.255.0"
```

and change the IP Address that the the VPN Clients are going through Pi-hole.

```
########################Pi-hole
push "dhcp-option DNS 192.168.1.76" # (< change the ip to your Pi IP)
###############################
```

and change the client's number depends on your needs:

```
max-clients 2
```
ctrl + x  
yes

&nbsp;

### DDClient-Dynamic DNS
```
sudo nano /etc/ddclient/ddclient.conf
```

Go to the bottom and change the "login=","password=", and the domain at the bottom.

&nbsp;

__Input required:__    
```
login=your-@emailaddress.com
password='your-password'
your-dyndns_domain
```

&nbsp;

### Easy-RSA
```
sudo pacman -S openvpn easy-rsa --noconfirm && sudo su
cd /etc/easy-rsa
export EASYRSA=$(pwd)
easyrsa init-pki
easyrsa build-ca nopass
```

&nbsp;

__Input required:__    
> Change 'your-dyndns_domain' to your domain.

```
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:your-dyndns_domain
Your new CA certificate file for publishing is at:/etc/easy-rsa/pki/ca.crt
cp /etc/easy-rsa/pki/ca.crt /etc/openvpn/server/
easyrsa gen-req ArchServer nopass
your-dyndns_domain
```

Enter

```
cp /etc/easy-rsa/pki/private/ArchServer.key /etc/openvpn/server/
openssl dhparam -out /etc/openvpn/server/dh.pem 2048
```

> This takes around 20 minutes

&nbsp;

__Input required:__    
> Change 'your-device' like Smartphone / Laptop etc.

```
openvpn --genkey --secret /etc/openvpn/server/ta.key
easyrsa gen-req your-device
```

Enter your-password and _NO COMMON NAME_!!
Hit enter

```
easyrsa sign-req server ArchServer
```

yes

&nbsp;

__Input required:__    
> Change 'your-device' like Smartphone / Laptop etc.

```
easyrsa sign-req client your-device
```

yes

```
mv /etc/easy-rsa/pki/issued/ArchServer.crt /etc/openvpn/server/
mkdir /etc/easy-rsa/pki/signed
mv /etc/easy-rsa/pki/issued/your-device.crt /etc/easy-rsa/pki/signed
exit
```

&nbsp;

### Client Config & ovpngen AUR
```
cd && pikaur -S ovpngen --noconfirm
```

&nbsp;

__Input required:__    
> Change 'your-dyndns_domain' to your domain.  
> Change 'your-device' like Smartphone / Laptop etc.

```
sudo ovpngen your-dyndns_domain /etc/openvpn/server/ca.crt /etc/easy-rsa/pki/signed/your-device.crt /etc/easy-rsa/pki/private/your-device.key /etc/openvpn/server/ta.key > your-device.ovpn
sudo nano your-device.ovpn
```

&nbsp;

__Input required:__    
> Change 'your-dyndns_domain' to your domain.  

```
remote your-dyndns_domain 1194 udp
```
and add behind ‘verb 3’

```
cipher AES-256-CBC
auth SHA512
resolv-retry infinite
tls-version-min 1.2
auth-nocache
remote-cert-tls server
comp-lzo
```
ctrl + x  
yes

&nbsp;

__Input required:__    
Copy the file to your Phone and import the file to the “OpenVPN for Android” application or to your computer.
```
sudo scp your-device.ovpn your-copmputer/your-user@192.168.1.xxx:/home/your-user/
```

&nbsp;

### New Clients

&nbsp;

__Input required:__    
> Change 'your-device' like Smartphone / Laptop etc.

```
sudo su && cd /etc/easy-rsa
easyrsa gen-req your-device
```

Enter your password and _NO COMMON NAME_!!  
Hit enter

&nbsp;

__Input required:__    
```
easyrsa sign-req client your-device
```

yes

&nbsp;

__Input required:__    
> Change 'your-dyndns_domain' to your domain.  
> Change 'your-device' like Smartphone / Laptop etc.

```
mv /etc/easy-rsa/pki/issued/your-device.crt /etc/easy-rsa/pki/signed
```
```
sudo ovpngen yourDYNDNSdomain.com /etc/openvpn/server/ca.crt /etc/easy-rsa/pki/signed/your-device.crt /etc/easy-rsa/pki/private/your-device.key /etc/openvpn/server/ta.key > your-device.ovpn
sudo nano your-device.ovpn
```

&nbsp;

__Input required:__    
> Change 'your-dyndns_domain' to your domain.  

```
remote your-dyndns_domain 1194 udp
```
and add behind ‘verb 3’

```
cipher AES-256-CBC
auth SHA512
resolv-retry infinite
tls-version-min 1.2
auth-nocache
remote-cert-tls server
comp-lzo
```
ctrl + x  
yes

&nbsp;

__Input required:__    
Copy the file to your Phone and import the file to the “OpenVPN for Android” application or to your computer.
```
sudo scp your-device.ovpn your-copmputer/your-user@192.168.1.xxx:/home/your-user/
```

&nbsp;

## 6. PHP7

&nbsp;

__Input required:__  
Uncomment the following lines in /etc/php/php.ini: (Delete ; )

```
sudo nano /etc/php/php.ini
date.timezone = your-location
```
ctrl + x  
yes

```
sudo systemctl restart php-fpm.service
```

&nbsp;

## 7. Adminer
```
sudo nano /etc/nginx/sites-available/adminer
```
Change the IP address to your server IP:

```
  server_name 192.168.1.76;
```
ctrl + x  
yes

```
sudo systemctl restart nginx.service
```

> check http://your-server_ip:22322/adminer

&nbsp;

## 8. msmtp
```
sudo nano /etc/msmtprc
```

&nbsp;

__Input required:__  
Change all "PwOSS", 'your-@emailaddress.com' and 'your-password' settings to your provider.

```
# PwOSS
account        pwoss
host           smtp.pwoss.xyz
port           587
from           yourEMAIL@address.com
user           yourEMAIL@address.com
password       YOUR password


# Set a default account
account default : pwoss
```
ctrl + x  
yes

&nbsp;

__Input required:__  
Test it
> Change email

```
echo "PwOSS - Server" | msmtp -a default your-@emailaddress.com
```
> Check your spam folder.

&nbsp;

## 9. raspiBackup
```
sudo mkdir /mnt/externalHD/backup/ && sudo chown pwoss:pwoss /mnt/externalHD/backup/ && sudo mkdir /mnt/externalHD/backup/myServer && sudo mkdir /mnt/externalHD/backup/myServer/raspiBackup && sudo nano /usr/local/etc/raspiBackup.conf
```

Change:
```
# email to send completion status
DEFAULT_EMAIL=""
```

to

&nbsp;

__Input required:__    
```
# email to send completion status
DEFAULT_EMAIL="your-@emailaddress.com"
```
ctrl + x  
yes

Test run:

```
sudo raspiBackup.sh
```

> First Backup will take around 10 minutes afterwards only 2-3 minutes.
to

&nbsp;

### Restore
Check with:

```
sudo fdisk -l | egrep "^Disk /|^/dev"
```

Restore example if it’s a USB Stick (incl SD Card):

```
sudo raspiBackup.sh -C -c -d /dev/sdb /mnt/externalHD/backup/myServer/raspiBackup/myServer/myServer-rsync-backup-2019******
```

&nbsp;

### Debug
```
sudo raspiBackup.sh -F -l debug
```

&nbsp;

## 10. Pi-hole
```
sudo nano /etc/nginx/sites-available/pihole
```

Change the IP address:

```
server_name 192.168.1.76; # Your server IP address
```
ctrl + x  
yes

&nbsp;

__Input required:__    
Set a password:
```
pihole -a -p
your-password
```

```
sudo nano /etc/hosts
```
Add to the bottom (change the IP to yours)

```
192.168.1.76 pi.hole myServer
```
ctrl + x  
yes

__Input required:__    
Change the home network IP (_192.168.1.0_)!!

```
sudo ufw allow from 192.168.1.0/24
```
```
sudo systemctl restart nginx.service
```

&nbsp;

### recursive DNS server (unbound)

__Input required:__    
Change _private-address: 192.168.1.0/16_ to your IP network:
```
sudo nano /etc/unbound/unbound.conf
```
ctrl + x  
yes

&nbsp;

## 11. Samba
Change password if you want. Otherwise keep going with the next step.  
> Just use samba with the pwoss passwords

&nbsp;

__Input required:__      
```
sudo smbpasswd -a pwoss
your-password
```

__Here is the next step!__ The following are the password and user for samba. Example for windows.

```
password = pwoss
user = pwoss
```

> check smb://your-server_ip/externalHD

&nbsp;

## 12. FreshRSS
```
sudo nano /etc/nginx/sites-available/freshrss
```

Change the IP to your server IP:

```
        server_name 192.168.1.76; # Your server IP address
```
ctrl + x  
yes

```
sudo systemctl restart nginx.service
```

> Check http://your-server_ip:7666 and follow the instructions.

> General Configuration - Your Admin User for the FreshRSS - server.

> Mysql Settings:  
> Database = FreshRSS  
> Database USER = FreshRSS  
> Password = pwoss  

&nbsp;

## 13. FireFox Sync Server
```
cd && cd software && cd syncserver && nano syncserver.ini
```

Change the IP address:

```
public_url = http://192.168.1.76:5000/
```
ctrl + x  
yes

A reboot is might be necessary:
```
sudo reboot now -h
```

> Check http://your-server_ip:5000/  
> it work's!  
> Is the answer!  

&nbsp;

### Clients
To configure desktop Firefox to talk to your new Sync server, go to “about:config”, search for “identity.sync.tokenserver.uri” and change its value to the URL of your server with a path of “token/1.0/sync/1.5”:  

- identity.sync.tokenserver.uri: http://sync.example.com/token/1.0/sync/1.5  

Alternatively, if you’re running your own Firefox Accounts server, and running Firefox 52 or later, see the documentation on how to Run your own Firefox Accounts Server for how to configure your client for both Sync and Firefox Accounts with a single preference.  

Since Firefox 33, Firefox for Android has supported custom sync servers. To configure Android Firefox 44 and later to talk to your new Sync server, just set the “identity.sync.tokenserver.uri” exactly as above before signing in to Firefox Accounts and Sync on your Android device.  

Important: after creating the Android account, changes to “identity.sync.tokenserver.uri” will be ignored. (If you need to change the URI, delete the Android account using the Settings > Sync > Disconnect... menu item, update the pref, and sign in again.) Non-default TokenServer URLs are displayed in the Settings > Sync panel in Firefox for Android, so you should be able to verify your URL there.  

Prior to Firefox 44, a custom add-on was needed to configure Firefox for Android. For Firefox 43 and earlier, see the blog post How to connect Firefox for Android to self-hosted Firefox Account and Firefox Sync servers.  

(Prior to Firefox 42, the TokenServer preference name for Firefox Desktop was “services.sync.tokenServerURI”. While the old preference name will work in Firefox 42 and later, the new preference is recommended as the old preference name will be reset when the user signs out from Sync causing potential confusion.)  

&nbsp;

## 14. Seafile - Server
```
sudo mkdir /mnt/externalHD/seafile && sudo chown -R seafile:seafile /mnt/externalHD/seafile
```
```
/sbin/ifconfig eth0 | grep 'inet 1' | awk '{ print $2}'
```

> Write the IP down

&nbsp;

### Seafile-Installation
```
sudo -u seafile -s /bin/sh
```
```
cd && wget https://github.com/haiwen/seafile-rpi/releases/download/v6.3.4/seafile-server_6.3.4_stable_pi.tar.gz && tar -xzf seafile-server_* && mv seafile-server_* installed && cd seafile-server-* && ./setup-seafile-mysql.sh
```
Enter
__Input required:__  

```
servername = myServer
ip = your-server_ip
/srv/seafile/seafile-data = /mnt/externalHD/seafile/seafile-data
```

Hit enter for “8082”
Hit 1
Hit enter for “localhost” and “3306”
> The mysql root user password is:  

```
pwoss
```

Hit enter for “mysql user”

__Input required:__  
```
Enter the password for mysql user "seafile": your-password
```

Hit enter for “[ ccnet database ]”
Hit enter for “[ seafile database ]”
Hit enter for “[ seahub database ]”
Enter through and wait until it’s done

```
./seafile.sh start
./seahub.sh start
```

__Input required:__  
> enter admin email

```
your-@emailaddress.com
```

__Input required:__  
> enter admin password

```
your-password
```
```
sudo systemctl restart seafile.service && sudo systemctl restart seahub.service
```
> A reboot is maybe necessary

```
sudo reboot now -h
```
> check http://your-server_ip:8000

&nbsp;

### SeafDav (WebDav)
```
sudo -u seafile -s /bin/sh
```
```
cd && cd conf && nano seafdav.conf
```
Change:

```
enabled = false
```
to

```
enabled = true
```
ctrl x & yes enter

```
cd && cd seafile-server-latest && ./seafile.sh restart && ./seahub.sh restart
```

> check http://your-server_ip:8080

&nbsp;

# 15. UFW
```
sudo nano /etc/ufw/before.rules
```

__Input required:__  
Add after header (# ufw-before-forward) and before (# Don't delete these required lines, otherwise there will be errors and change the '_your-interface_'

```
# NAT (Network Address Translation) table rules
*nat
:POSTROUTING ACCEPT [0:0]

# Allow traffic from clients to the interface
-A POSTROUTING -s 10.8.0.0/24 -o your-interface -j MASQUERADE

# do not delete the "COMMIT" line or the NAT table rules above will not be processed
COMMIT
```

&nbsp;

## 16. Swap file

> We could add the file to the image straightaway but we like to keep the image small.  
You don't need to have a swap file but it is good to have to get a little bit more "power" for your Raspberry Pi.

```
sudo fallocate -l 512M /swapfile && chmod 600 /swapfile && mkswap /swapfile && swapon /swapfile
```
```
sudo nano /etc/fstab
```
Add to the bottom of the fstab list:

```
/swapfile none swap defaults 0 0
```
ctrl + x  
yes

&nbsp;

## (Optional) - If you want to change the boot text
```
sudo nano /etc/motd
```
```
################################################
Welcome to your PwOSS-Server

     Website: https://pwoss.xyz
       Forum: https://forum.pwoss.xyz
       Guide: https://guideline.pwoss.xyz
################################################
This image is based on Arch Linux | ARM

     Website: http://archlinuxarm.org
       Forum: http://archlinuxarm.org/forum
         IRC: #archlinux-arm on irc.Freenode.net
################################################
```

&nbsp;

## REBOOT
```
sudo reboot now -h
```

&nbsp;
&nbsp;

## Your servers are running...

&nbsp;

- Radicale = Contact and Calendar Server ----> http://your-server_ip:5232  
- Seafile = Cloud Server ----> http://your-server_ip:8000  
- WebDav = WebDav Server ----> http://your-server_ip:8080  
- VPN = Virtual Private Network ----> your-dyndns_domain
- Samba = File Server ----> smb://your-server_ip/externalHD  
- FireFox = Sync Bookmarks/History ----> http://your-server_ip:5000/  
- Pi-hole = Advertising blocker ----> http://your-server_ip:987/admin/  
- FreshRSS = RSS Reader ----> http://your-server_ip:7666  

&nbsp;

Now you’re able to save your personal data on your own server. To keep it safe against a burglar, natural disasters, hardware defects we suggest to set up the same or similar server with a friend or family member.

&nbsp;

## __ENJOY__
