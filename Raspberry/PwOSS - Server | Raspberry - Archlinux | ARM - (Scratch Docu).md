## PwOSS - Server | Raspberry - Archlinux | ARM
(Scratch Docu)

&nbsp;

### Important - before you start check following:

1. Your router needs the possibility of port forwarding and the possibility to configure the DNS server for Pi-hole.  
2. You’ll need a DynDNS-Domain. For example, at https://www.noip.com/sign-up.  
3. You have to connect a USB Stick/External HardDrive  

&nbsp;

### Info / Tip
Some commands must be changed by you. The keywords will start with '__your-__'.
- your-interface
- your-password
- your-location
- etc.

We will mark it with the words '__Input required:__  ' above the commands.

Hit the tab key for autocompletion when typing commands.

&nbsp;

Get the image from [archlinuxarm.org](https://archlinuxarm.org/platforms/armv7/broadcom/raspberry-pi-2#installation) and follow the instructions.

&nbsp;

### For SSH without keyboard. Connection from PC/Smartphone per Terminal to Raspberry - Arch

You have to find your local IP address of your raspberry pi to connect per SSH to the terminal.

Terminal command:

```
arp -n | awk '/b8:27:eb/ {print $1}'
```

In my constellation, it's 192.168.1.76. You can find it on your router as well.

&nbsp;

Open your terminal and type:
```
ssh alarm@192.168.1.76
alarm
su
root
```

> You have to use the IP quite often. Write it down.

&nbsp;

## 1. Update System, Keys etc. first
```
pacman -Sy archlinux-keyring && pacman-key --init && pacman-key --populate archlinuxarm && pacman -Syu
```

&nbsp;

## 2. Change TimeZone

```
timedatectl list-timezones
```

Choose your timezone and copy it.  
ctrl z

&nbsp;

__Input required:__  
```
timedatectl set-timezone your-location
```
ctrl + x  
yes

&nbsp;

## 3. Add another user

&nbsp;

__Input required:__  
```
useradd -m -G wheel -s /bin/bash pwoss
passwd
your-password
```

&nbsp;

## 3.1 (Optional) Delete user alarm (archlinux | ARM)
```
userdel -r alarm
```

&nbsp;

## 4. Add user to sudo
```
pacman -S sudo --noconfirm && visudo
```

Uncomment:

```
# %wheel ALL=(ALL) ALL
```

to

```
%wheel ALL=(ALL) ALL
```
ctrl + x  
yes

```
su - pwoss
```

&nbsp;

## 5. pikaur - AUR-Helper
```
sudo pacman -S packer git base-devel
```

> Enter (default=all)

```
cd && mkdir software && cd software && git clone https://github.com/actionless/pikaur.git && cd pikaur && makepkg -fsri --noconfirm
```

&nbsp;

## 6 downgrade
```
pikaur -S downgrade --noconfirm
```

&nbsp;

## 7. Crontab
```
sudo pacman -S cronie --noconfirm && sudo systemctl enable cronie.service && sudo systemctl start cronie.service
```

&nbsp;

## 8. Change editor to nano
```
sudo nano /etc/environment
```
Paste under the lines:

```
export EDITOR=/usr/bin/nano
```
ctrl + x  
yes

&nbsp;

## 9. Changing Hostname and Hosts
```
sudo nano /etc/hostname
```
Delete alarmpi and add:

```
myServer
```
ctrl + x  
yes

```
sudo nano /etc/hosts
```
Paste under the lines:

```
127.0.0.1 localhost
127.0.1.1 myserver.localdomain myServer
```
ctrl + x  
yes

&nbsp;

## 10. MariaDB
```
sudo pacman -S mariadb --noconfirm && sudo mysql_install_db --user=mysql --basedir=/usr/ --ldata=/var/lib/mysql/ && sudo systemctl enable mariadb.service && sudo systemctl start mariadb.service && sudo mysql_secure_installation
```

Hit enter and set up the mysql root password (use a good password) and hit the following enter for yes.

&nbsp;

## 11. dm-crypt LUKS
```
sudo cryptsetup -v luksFormat /dev/sda
```

YES  

&nbsp;

__Input required:__  
passphrase=your-password

```
sudo cryptsetup luksOpen /dev/sda externalHD
```

&nbsp;

__Input required:__  
passphrase=your-password

```
sudo mkfs.ext4 /dev/mapper/externalHD
sudo mkdir /mnt/externalHD
sudo cryptsetup luksClose externalHD
sudo dd if=/dev/urandom of=/home/pwoss/.key bs=4096 count=4
sudo chmod 400 /home/pwoss/.key
sudo cryptsetup luksAddKey /dev/sda /home/pwoss/.key
```

&nbsp;

__Input required:__  
passphrase=your-password

```
sudo nano /etc/crypttab
```

Add to the bottom:

```
externalHD /dev/sda /home/pwoss/.key luks
```
ctrl + x  
yes

```
sudo nano /etc/fstab
```

Add to the bottom:

```
/dev/mapper/externalHD /mnt/externalHD ext4 defaults 0 0
```
ctrl + x  
yes

Do a reboot.

```
sudo reboot now -h
```

&nbsp;

## 12. Seafile – Server

&nbsp;

### Needed packages

```
sudo pacman -S fuse2 libarchive vala libevent libldap libmariadbclient python2-chardet python2-dateutil python2-django python-flup python2-gunicorn python2-memcached python2-openpyxl python2-pillow python2-pytz python2-requests python2-requests-oauthlib python2-six mysql-python wget --noconfirm
```
```
pikaur -S libevhtp-seafile libsearpc python2-qrcode python2-cas python2-django-compressor python2-django-constance python2-django-picklefield python2-django-post-office python2-django-rest-framework python2-django-simple-captcha python2-django-statici18n python2-django-webpack-loader python2-django-pylibmc python2-wsgidav-seafile libselinux --noconfirm
```

> Enter Y (Yes) for everything.

&nbsp;

### Seafile User & Seafile-Data path
```
sudo useradd -m -r -d /srv/seafile -s /usr/bin/nologin seafile && sudo mkdir /mnt/externalHD/seafile && sudo chown -R seafile:seafile /mnt/externalHD/seafile
```

&nbsp;

### Seafile-Installation
```
sudo -u seafile -s /bin/sh
```
```
cd && mkdir installed && wget https://github.com/haiwen/seafile-rpi/releases/download/v6.3.4/seafile-server_6.3.4_stable_pi.tar.gz && tar -xzf seafile-server_* && mv seafile-server_* installed && cd seafile-server-* && ./setup-seafile-mysql.sh
```

Enter

&nbsp;

__Input required:__  
```
servername = myServer
ip = your-server_ip
/srv/seafile/seafile-data = /mnt/externalHD/seafile/seafile-data
```

Hit enter for “8082”  
Hit 1  
Hit enter for “localhost” and “3306”  
> What is the password of the mysql root user?  

&nbsp;

__Input required:__  
```
your-password
```

Hit enter for “mysql user”

&nbsp;

__Input required:__  
```
Enter the password for mysql user "seafile": your-password
```

Hit enter for “[ ccnet database ]”  
Hit enter for “[ seafile database ]”  
Hit enter for “[ seahub database ]”  
Enter through and wait until it’s done  

```
./seafile.sh start
./seahub.sh start
```

&nbsp;

__Input required:__  
> enter admin email

```
your-@emailaddress.com
```

&nbsp;

__Input required:__  
> enter admin password

```
your-password
```

&nbsp;

> python2-urllib3 downgrade is still necessary to _python2-urllib3-1.23-2_
> [PwOSS - Link](https://pwoss.xyz/downgrade-seafile-server-internal-server-error-couldnt-load-libraries/)

```
downgrade python2-urllib3
```

&nbsp;

### Seafile Server Autostart
```
sudo nano /etc/systemd/system/seafile.service
```
```
[Unit]
Description=Seafile
# add mysql.service or postgresql.service depending on your database to the line below
After=network-online.target network.target mariadb.service

[Service]
Type=oneshot
ExecStart=/srv/seafile/seafile-server-latest/seafile.sh start
ExecStop=/srv/seafile/seafile-server-latest/seafile.sh stop
RemainAfterExit=yes
User=seafile
Group=seafile

[Install]
WantedBy=multi-user.target
```
ctrl + x  
yes

```
sudo nano /etc/systemd/system/seahub.service
```
```
[Unit]
Description=Seafile hub
After=network-online.target network.target seafile.service

[Service]
# change start to start-fastcgi if you want to run fastcgi
ExecStart=/srv/seafile/seafile-server-latest/seahub.sh start
ExecStop=/srv/seafile/seafile-server-latest/seahub.sh stop
User=seafile
Group=seafile
Type=oneshot
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target
```
ctrl + x  
yes

```
sudo systemctl enable seafile.service && sudo systemctl enable seahub.service
```

&nbsp;

### SeafDav (WebDav)
```
cd && cd conf && nano seafdav.conf
```

Change:

```
enabled = false
```

to

```
enabled = true
```
ctrl + x  
yes

```
cd && cd seafile-server-latest && ./seafile.sh restart && ./seahub.sh restart
```

&nbsp;

## 13. Radicale ( Calendar & Contact Server)
```
sudo pacman -S radicale python-setuptools --noconfirm && su
```

> The root user password

```
mkdir -p /var/lib/radicale/collections && chown -R radicale:radicale /var/lib/radicale/collections && chmod -R o= /var/lib/radicale/collections && nano /etc/systemd/system/radicale.service
```

Hit enter until the nano editor window pop up and add:

```
[Unit]
Description=A simple CalDAV (calendar) and CardDAV (contact) server
After=network.target
Requires=network.target

[Service]
ExecStart=/usr/bin/env python3 -m radicale
Restart=on-failure
User=radicale

# Deny other users access to the calendar data
UMask=0027

# Optional security settings
PrivateTmp=true
ProtectSystem=strict
ProtectHome=true
PrivateDevices=true
ProtectKernelTunables=true
ProtectKernelModules=true
ProtectControlGroups=true
NoNewPrivileges=true
ReadWritePaths=/var/lib/radicale/collections

[Install]
WantedBy=multi-user.target
```
ctrl + x  
yes

```
nano /etc/radicale/config
```

Change:

```
# hosts = 127.0.0.1:5232
```
to

```
hosts = 192.168.1.76:5232
```
and the following too

```
# type = none
type = htpasswd
# htpasswd_filename = /etc/radicale/users
htpasswd_filename = /etc/radicale/users
# htpasswd_encryption = bcrypt
htpasswd_encryption = plain
# delay = 1
delay = 1
# max_connections = 20
max_connections = 20
# max_content_length = 10000000
max_content_length = 10000000
# timeout = 10
timeout = 10
```
ctrl + x  
yes

```
nano /etc/radicale/users
```

&nbsp;

__Input required:__  
> Change to your Family Member for example.

```
your-user1:your-user1_password
your-user2:your-user2_password
...
```
ctrl + x  
yes

```
systemctl enable radicale && systemctl start radicale && systemctl status radicale
```

```
su - pwoss
```

&nbsp;

## 14. OpenVPN
```
sudo groupadd nogroup
```

&nbsp;

### DDClient-Dynamic DNS
```
sudo pacman -S ddclient --noconfirm && sudo nano /etc/ddclient/ddclient.conf
```

Add following lines and change the "login=","password=", and the domain at the bottom.

&nbsp;

__Input required:__  
```
For noip
protocol=dyndns2
use=web, if=eth0
server=dynupdate.no-ip.com
login=your-@emailaddress.com
password='your-password'
your-dyndns_domain
```
```
sudo crontab -e
```

add

```
#######################DDClient
45 04 * * * /usr/sbin/ddclient --force
##################################
```

&nbsp;

### Easy-RSA
```
sudo pacman -S openvpn easy-rsa --noconfirm && sudo su
cd /etc/easy-rsa
export EASYRSA=$(pwd)
easyrsa init-pki
easyrsa build-ca nopass
```

&nbsp;

__Input required:__  
> Change 'your-dyndns_domain' to your domain.

```
Common Name (eg: your user, host, or server name) [Easy-RSA CA]:your-dyndns_domain
Your new CA certificate file for publishing is at:/etc/easy-rsa/pki/ca.crt
cp /etc/easy-rsa/pki/ca.crt /etc/openvpn/server/
easyrsa gen-req ArchServer nopass
your-dyndns_domain
```

Enter

```
cp /etc/easy-rsa/pki/private/ArchServer.key /etc/openvpn/server/
openssl dhparam -out /etc/openvpn/server/dh.pem 2048
```

> This takes around 20 minutes

&nbsp;

__Input required:__  
> Change 'your-device' like Smartphone / Laptop etc.

```
openvpn --genkey --secret /etc/openvpn/server/ta.key
easyrsa gen-req your-device
```

Enter your-password and _NO COMMON NAME_!!
Hit enter

```
easyrsa sign-req server ArchServer
```

yes

&nbsp;

__Input required:__  
> Change 'your-device' like Smartphone / Laptop etc.
```
easyrsa sign-req client your-device
```

yes

```
mv /etc/easy-rsa/pki/issued/ArchServer.crt /etc/openvpn/server/
mkdir /etc/easy-rsa/pki/signed
mv /etc/easy-rsa/pki/issued/your-device.crt /etc/easy-rsa/pki/signed
exit
```

&nbsp;

### Client Config & ovpngen AUR
```
cd && pikaur -S ovpngen --noconfirm
```

&nbsp;

__Input required:__  
> Change 'your-dyndns_domain' to your domain.  
> Change 'your-device' like Smartphone / Laptop etc.

```
sudo ovpngen your-dyndns_domain /etc/openvpn/server/ca.crt /etc/easy-rsa/pki/signed/your-device.crt /etc/easy-rsa/pki/private/your-device.key /etc/openvpn/server/ta.key > your-device.ovpn
sudo nano your-device.ovpn
```

&nbsp;

__Input required:__  
> Change 'your-dyndns_domain' to your domain.  

```
remote your-dyndns_domain 1194 udp
```
and add behind ‘verb 3’

```
cipher AES-256-CBC
auth SHA512
resolv-retry infinite
tls-version-min 1.2
auth-nocache
remote-cert-tls server
comp-lzo
```
ctrl + x  
yes

&nbsp;

__Input required:__  
Copy the file to your Phone and import the file to the “OpenVPN for Android” application or to your computer.
```
sudo scp your-device.ovpn your-copmputer/your-user@192.168.1.xxx:/home/your-user/
```

&nbsp;

### Server-Config
```
sudo nano /etc/openvpn/server/server.conf
```

> Change the IP to your home network if it’s necessary.

```
# your local subnet
push "route 192.168.1.0 255.255.255.0"
```

Change the client's number depends on your needs:

```
max-clients 2
```
```
port 1194
proto udp
dev tun
ca /etc/openvpn/server/ca.crt
cert /etc/openvpn/server/ArchServer.crt
key /etc/openvpn/server/ArchServer.key
dh /etc/openvpn/server/dh.pem
server 10.8.0.0 255.255.255.0
# server and remote endpoints
ifconfig 10.8.0.1 10.8.0.2
# Add route to Client routing table for the OpenVPN Server
push "route 10.8.0.1 255.255.255.255"
# Add route to Client routing table for the OPenVPN Subnet
push "route 10.8.0.0 255.255.255.0"
# your local subnet
push "route 192.168.1.0 255.255.255.0"
# Set your primary domain name server address for clients

########################Pi-hole
push "dhcp-option DNS 192.168.1.76"
###############################

###### https://dns.watch/
#push "dhcp-option DNS 84.200.69.80"
#push "dhcp-option DNS 84.200.70.40"
# Override the Client default gateway by using 0.0.0.0/1 and
# 128.0.0.0/1 rather than 0.0.0.0/0. This has the benefit of
# overriding but not wiping out the original default gateway.
#push "redirect-gateway def1"
push "redirect-gateway def1 bypass-dhcp"

client-to-client
duplicate-cn
keepalive 10 120
tls-version-min 1.2
tls-auth /etc/openvpn/server/ta.key 0
cipher AES-256-CBC
auth SHA512
#comp-lzo
compress lz4-v2
push "compress lz4-v2"
user nobody
group nogroup
persist-key
persist-tun
max-clients 2
remote-cert-tls client
#client-connect /etc/openvpn/vpn-connect.sh
#client-disconnect /etc/openvpn/vpn-disconnect.sh
#script-security 2
#crl-verify /etc/openvpn/crl.pem
status /var/log/openvpn-status.log 20
status-version 3
log /var/log/openvpn.log
verb 3
ifconfig-pool-persist ipp.txt
log-append /var/log/openvpn
status /tmp/vpn.status 10
```
ctrl + x  
yes

```
sudo systemctl enable openvpn-server@server.service && sudo systemctl start openvpn-server@server.service
```

&nbsp;

### New Clients

&nbsp;

__Input required:__  
> Change 'your-device' like Smartphone / Laptop etc.

```
sudo su && cd /etc/easy-rsa
easyrsa gen-req your-device
```

Enter your password and _NO COMMON NAME_!!  
Hit enter

&nbsp;

__Input required:__  
```
easyrsa sign-req client your-device
```

yes

&nbsp;

__Input required:__  
> Change 'your-dyndns_domain' to your domain.  
> Change 'your-device' like Smartphone / Laptop etc.

```
mv /etc/easy-rsa/pki/issued/your-device.crt /etc/easy-rsa/pki/signed
```
```
sudo ovpngen yourDYNDNSdomain.com /etc/openvpn/server/ca.crt /etc/easy-rsa/pki/signed/your-device.crt /etc/easy-rsa/pki/private/your-device.key /etc/openvpn/server/ta.key > your-device.ovpn
sudo nano your-device.ovpn
```

&nbsp;

__Input required:__  
> Change 'your-dyndns_domain' to your domain.  

```
remote your-dyndns_domain 1194 udp
```
and add behind ‘verb 3’

```
cipher AES-256-CBC
auth SHA512
resolv-retry infinite
tls-version-min 1.2
auth-nocache
remote-cert-tls server
comp-lzo
```
ctrl + x  
yes

&nbsp;

__Input required:__  
Copy the file to your Phone and import the file to the “OpenVPN for Android” application or to your computer.
```
sudo scp your-device.ovpn your-copmputer/your-user@192.168.1.xxx:/home/your-user/
```

&nbsp;

## 15. UFW
```
sudo pacman -S ufw --noconfirm && sudo nano /etc/default/ufw
```

Change:

```
DEFAULT_FORWARD_POLICY="DROP"
```

to

```
DEFAULT_FORWARD_POLICY="ACCEPT"
```
ctrl + x  
yes

```
sudo nano /etc/ufw/before.rules
```

&nbsp;

__Input required:__  
Add after header (# ufw-before-forward) and before (# Don't delete these required lines, otherwise there will be errors and change the '_your-interface_'

```
# NAT (Network Address Translation) table rules
*nat
:POSTROUTING ACCEPT [0:0]

# Allow traffic from clients to the interface
-A POSTROUTING -s 10.8.0.0/24 -o your-interface -j MASQUERADE

# do not delete the "COMMIT" line or the NAT table rules above will not be processed
COMMIT
```
ctrl + x  
yes

```
sudo ufw allow ssh && sudo ufw allow 1194/udp && sudo ufw allow 8000/tcp && sudo ufw allow 8080/tcp && sudo ufw allow 8082/tcp && sudo ufw allow 5232/tcp
```

y

```
sudo nano /etc/ufw/sysctl.conf
```

Uncomment:

```
#net/ipv4/ip_forward=1
```

to

```
net/ipv4/ip_forward=1
```
ctrl + x  
yes

```
sudo ufw enable && sudo systemctl enable ufw.service && sudo systemctl start ufw.service
```
YES

&nbsp;

## 16. Bash Completion
```
sudo pacman -S bash-completion --noconfirm && nano ~/.bashrc
```
Add to the bottom:

```
if [ -f /etc/bash_completion ]; then          
. /etc/bash_completion
fi
export EDITOR=/usr/bin/nano
export VISUAL=$EDITOR
```
ctrl + x  
yes

&nbsp;

## 17. Nginx Mainline
```
sudo pacman -S nginx-mainline --noconfirm && sudo nano /etc/nginx/nginx.conf
```
Change _worker_processes  1;_    
to

```
worker_processes  4;
```
And add to the bottom one line before  
_}_

```
include sites-enabled/*; # See Server blocks
```
ctrl + x  
yes

```
sudo mkdir /etc/nginx/sites-available && sudo mkdir /etc/nginx/sites-enabled && sudo systemctl enable nginx.service && sudo systemctl start nginx.service
```

&nbsp;

## 18. PHP7
```
sudo pacman -S php php-fpm php-gd php-sqlite --noconfirm
```

&nbsp;

__Input required:__  
Uncomment the following lines in /etc/php/php.ini: (Delete ; )

```
sudo nano /etc/php/php.ini
date.timezone = your-location
```
Change:

```
;open_basedir =
```
to

```
open_basedir = /srv/http/:/home/:/tmp/:/usr/share/pear/:/usr/share/webapps/
```
```
...
extension=pdo_sqlite
extension=sockets
extension=sqlite3
extension=pdo_mysql
extension=mysqli
extension=gd
```
and change:

```
expose_php = On
```
to

```
expose_php = Off
```
ctrl + x  
yes

```
sudo systemctl enable php-fpm.service && sudo systemctl start php-fpm.service
```

&nbsp;

## 19. Adminer
```
pikaur -S adminer --noconfirm && sudo nano /etc/nginx/sites-available/adminer
```
Add the following lines and change the IP address to your server IP

```
server {
  listen 22322;
  server_name 192.168.1.76;

  root /usr/share/webapps/adminer;

# If you want to use a .htpass file, uncomment the three following lines.
#auth_basic "Admin-Area! Password needed!";
#auth_basic_user_file /usr/share/webapps/adminer/.htpass;
#access_log /var/log/nginx/adminer-access.log;

error_log /var/log/nginx/adminer-error.log;

location / {
  index index.php;
  try_files $uri $uri/ /index.php?$args;
  }

  location ~ .php$ {
    include fastcgi.conf;
#    fastcgi_pass localhost:9000;
    fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
    fastcgi_index index.php;
    fastcgi_param SCRIPT_FILENAME /usr/share/webapps/adminer$fastcgi_script_name;
  }
}
```
ctrl + x  
yes

```
sudo ln -s /etc/nginx/sites-available/adminer /etc/nginx/sites-enabled/ && sudo ufw allow 22322/tcp && sudo systemctl restart nginx.service
```
> check http://your-server_ip:22322/adminer

&nbsp;

## 20. msmtp
```
sudo pacman -S msmtp msmtp-mta --noconfirm && sudo nano /etc/msmtprc
```

&nbsp;

__Input required:__  
Add and change all "PwOSS", 'your-@emailaddress.com' and 'your-password' settings to your provider.

```
# Set default values for all following accounts.
defaults
auth           on
tls            on
tls_trust_file /etc/ssl/certs/ca-certificates.crt
logfile        ~/.msmtp.log

# PwOSS
account        pwoss
host           smtp.pwoss.xyz
port           587
from           your-@emailaddress.com
user           your-@emailaddress.com
password       your-password


# Set a default account
account default : pwoss
```
ctrl + x  
yes

If you want to get info/emails from your crontab add the following line:

```
sudo nano /usr/lib/systemd/system/cronie.service
```
Change:

```
ExecStart=/usr/bin/crond -n
```
to

```
ExecStart=/usr/bin/crond -n -m '/usr/bin/msmtp -t'
```
ctrl + x  
yes

```
sudo systemctl daemon-reload && sudo systemctl restart cronie.service
```

&nbsp;

__Input required:__  
Test it
> Change email

```
echo "PwOSS - Server" | msmtp -a default your-@emailaddress.com
```
> Check your spam folder.

&nbsp;

## 21. raspiBackup
```
sudo pacman -S parted dosfstools rsync --noconfirm && sudo curl -s -L -O https://www.linux-tips-and-tricks.de/raspiBackupInstall.sh && sudo bash raspiBackupInstall.sh
```

> DE or EN  
> normal or partition? It means if the pi is running on noob or similar options you have to choose p. Normal is sd or external hard drives -> N

```
RSYNC
20
J
j
```
```
sudo mkdir /mnt/externalHD/backup/ && sudo chown pwoss:pwoss /mnt/externalHD/backup/ && sudo mkdir /mnt/externalHD/backup/myServer && sudo mkdir /mnt/externalHD/backup/myServer/raspiBackup && sudo nano /usr/local/etc/raspiBackup.conf
```

Change following and add your services:

```
DEFAULT_BACKUPPATH="/backup"
```

to

```
DEFAULT_BACKUPPATH="/mnt/externalHD/backup/myServer/raspiBackup"
```

and

```
DEFAULT_STOPSERVICES=””
```

to

```
DEFAULT_STOPSERVICES="systemctl stop nginx.service && systemctl stop mariadb.service && systemctl stop seahub.service && systemctl stop seafile.service && systemctl stop openvpn-server@server.service && systemctl stop php-fpm.service && systemctl stop radicale.service"
```

and

```
DEFAULT_STARTSERVICES=””
```

to

```
DEFAULT_STARTSERVICES="systemctl start mariadb.service && systemctl start nginx.service && systemctl start seafile.service && systemctl start openvpn-server@server.service && systemctl start seahub.service && systemctl start php-fpm.service && systemctl start radicale.service"
```
```
# email to send completion status
DEFAULT_EMAIL=""
```

to

&nbsp;

__Input required:__  
```
# email to send completion status
DEFAULT_EMAIL="your-@emailaddress.com"
```

and

```
# mailprogram
DEFAULT_MAIL_PROGRAM="mail"
```

> to

```
# mailprogram
DEFAULT_MAIL_PROGRAM="msmtp"
```
ctrl + x  
yes

```
sudo crontab -e
```

Add to the bottom:

```
#######################raspiBackup
00 05 * * * /usr/local/bin/raspiBackup.sh
##################################
```
ctrl + x  
yes

Test run:

```
sudo raspiBackup.sh
```

> First Backup will take around 10 minutes afterwards only 2-3 minutes

&nbsp;

### Restore
Check with:

```
sudo fdisk -l | egrep "^Disk /|^/dev"
```

Restore example if it’s a USB Stick (incl SD Card):

```
sudo raspiBackup.sh -C -c -d /dev/sdb /mnt/externalHD/backup/myServer/raspiBackup/myServer/myServer-rsync-backup-2019******
```

&nbsp;

### Debug
```
sudo raspiBackup.sh -F -l debug
```

&nbsp;

## 22. Pi-hole
```
pikaur -S pi-hole-server --noconfirm && sudo nano /etc/resolvconf.conf
```
Uncomment:

```
#name_servers=127.0.0.1
```
to

```
name_servers=127.0.0.1
```
ctrl + x  
yes

```
sudo resolvconf -u
```
```
sudo nano /etc/hosts
```
Add to the bottom (change the IP to yours)

```
192.168.1.76 pi.hole myServer
```
```
sudo nano /etc/nginx/nginx.conf
```
Change:

```
#gzip  on;
```
to

```
gzip  on;
```
and add under gzip on;

```
  gzip_min_length 1000;
  gzip_proxied    expired no-cache no-store private auth;
  gzip_types      text/plain application/xml application/json application/javascript application/octet-stream text/css;
  include /etc/nginx/conf.d/*.conf;
```
```
sudo cp /usr/share/pihole/configs/nginx.example.conf /etc/nginx/sites-available/pihole && sudo nano /etc/nginx/sites-available/pihole
```
and change:

```
listen 80 default_server;
       listen [::]:80 default_server;
server_name _;
```
to

```
listen 987 default_server;
       listen [::]:987 default_server;
server_name 192.168.1.76; # Your server IP address
```
and change:

```
              fastcgi_pass  127.0.0.1:9000;
```
to

```              
              fastcgi_pass unix:/run/php-fpm/php-fpm.sock;
```
ctrl + x  
yes

```
sudo ln -s /etc/nginx/sites-available/pihole /etc/nginx/sites-enabled/ && sudo nano /etc/php/php.ini
```
Add behind the others _/srv/http/:/home/:/tmp/:/usr/share/pear/:/usr/share/webapps/_

```
:/srv/http/pihole:/run/pihole-ftl/pihole-FTL.port:/run/log/pihole/pihole.log:/run/log/pihole-ftl/pihole-FTL.log:/etc/pihole:/etc/hosts:/etc/hostname:/etc/dnsmasq.d:/proc/meminfo:/proc/cpuinfo:/sys/class/thermal/thermal_zone0/temp:/dev/null
```
ctrl + x  
yes

&nbsp;

__Input required:__  
Set a password:
```
pihole -a -p
your-password
```
```
sudo nano /etc/dnsmasq.d/00-openvpn.conf
```
add

```
interface=tun0
```
ctrl + x  
yes

```
sudo ufw allow 987/tcp && sudo ufw allow from 10.8.0.0/24
```
```
sudo crontab -e
```
add

```
#######################pihole flush logs
45 23 * * 0,3 pihole -f
################################


#######################pihole update new blocks
15 23 * * 0,3 pihole -g
################################
```
ctrl + x  
yes

```
sudo nano /etc/openvpn/server/server.conf
```
Change the VPN route through Pi-hole and change the IP Address

```
########################Pi-hole
#push "dhcp-option DNS 192.168.1.76"
###############################
```
to

```
########################Pi-hole
push "dhcp-option DNS 192.168.1.76" # (< change the IP to your server IP)
###############################
```
ctrl + x  
yes

__Input required:__  
Change the home network IP (_192.168.1.0_)!!

```
sudo ufw allow from 192.168.1.0/24
```
```
sudo systemctl stop systemd-resolved.service && sudo systemctl disable systemd-resolved.service && sudo systemctl restart pihole-FTL.service && sudo systemctl restart nginx.service && sudo systemctl restart php-fpm.service
```
> check http://your-server_ip:987/admin

&nbsp;

### recursive DNS server (unbound)
```
sudo pacman -S unbound expat --noconfirm && wget -O root.hints https://www.internic.net/domain/named.cache && sudo mv root.hints /etc/unbound/ && sudo mv /etc/unbound/unbound.conf /etc/unbound/unbound.conf.backup && sudo nano /etc/unbound/unbound.conf
```
__Input required:__  
Add the following and change _private-address: 192.168.1.0/16_ to your IP network.

```
server:
    # If no logfile is specified, syslog is used
    # logfile: "/var/log/unbound/unbound.log"
    verbosity: 0

    port: 5353
    do-ip4: yes
    do-udp: yes
    do-tcp: yes
    do-daemonize: no
    trust-anchor-file: trusted-key.key

    # May be set to yes if you have IPv6 connectivity
    do-ip6: no

    # Use this only when you downloaded the list of primary root servers!
    root-hints: "/etc/unbound/root.hints"

    # Trust glue only if it is within the servers authority
    harden-glue: yes

    # Require DNSSEC data for trust-anchored zones, if such data is absent, the zone becomes BOGUS
    harden-dnssec-stripped: yes

    # Don't use Capitalization randomization as it known to cause DNSSEC issues sometimes
    # see https://discourse.pi-hole.net/t/unbound-stubby-or-dnscrypt-proxy/9378 for further details
    use-caps-for-id: no

    # Reduce EDNS reassembly buffer size.
    # Suggested by the unbound man page to reduce fragmentation reassembly problems
    edns-buffer-size: 1472

    # TTL bounds for cache
    cache-min-ttl: 3600
    cache-max-ttl: 86400

    # Perform prefetching of close to expired message cache entries
    # This only applies to domains that have been frequently queried
    prefetch: yes

    # One thread should be sufficient, can be increased on beefy machines
    num-threads: 1

    # Ensure kernel buffer is large enough to not loose messages in traffic spikes
    so-rcvbuf: 1m

    # Ensure privacy of local IP ranges
    private-address: 192.168.1.0/16
    private-address: 10.0.0.0/8
    #private-address: fd00::/8 # IPv6
    #private-address: fe80::/10 # IPv6
```
ctrl + x  
yes

```
sudo systemctl enable unbound.service && sudo systemctl start unbound.service
```
```
sudo nano /etc/systemd/system/roothints.service
```
```
[Unit]
Description=Update root hints for unbound
After=network.target

[Service]
ExecStart=/usr/bin/curl -o /etc/unbound/root.hints https://www.internic.net/domain/named.cache
```
ctrl + x  
yes

```
sudo nano /etc/systemd/system/roothints.timer
```
```
[Unit]
Description=Run root.hints monthly

[Timer]
OnCalendar=monthly
Persistent=true

[Install]
WantedBy=timers.target
```
ctrl + x  
yes

```
sudo systemctl enable roothints.timer && sudo systemctl start roothints.timer
```

You need to change the settings of your Pi-hole.  
Go to _http://192.168.1.76:987/admin/settings.php?tab=dns_ and disable all DNS server on the left side and add to _Custom 1 (IPv4)_

```
127.0.0.1#5353
```
and save it.

### dnscrypt-proxy
```
sudo pacman -S dnscrypt-proxy && sudo nano /etc/dnscrypt-proxy/dnscrypt-proxy.toml
```
Change
```
# server_names = ['scaleway-fr', 'google', 'yandex', 'cloudflare']
listen_addresses = ['127.0.0.1:53', '[::1]:53']
```
to
```
server_names = ['dnscrypt.me', 'de.dnsmaschine.net', 'doh-crypto-sx', 'scaleway-fr']
listen_addresses = ['127.0.0.1:53000', '[::1]:53000']
```
ctrl + x  
yes

```
sudo nano /etc/unbound/unbound.conf
```
Add the following under the other lines.
```
# dnscrypt-proxy
  do-not-query-localhost: no
forward-zone:
  name: "."
  forward-addr: ::1@53000
  forward-addr: 127.0.0.1@53000
```
ctrl + x  
yes

```
sudo systemctl enable dnscrypt-proxy.service && sudo systemctl start dnscrypt-proxy.service && sudo systemctl restart unbound.service
```

&nbsp;

## 23. Samba
```
sudo pacman -S samba --noconfirm && sudo nano /etc/samba/smb.conf
```
Add:

```
[global]
workgroup = WORKGROUP
security = user
encrypt passwords = yes

[externalHD]
comment = externalHD
path = /mnt/externalHD/backup
read only = no
```
ctrl + x  
yes

&nbsp;

__Input required:__    
```
sudo smbpasswd -a pwoss
your-password
```
```
sudo ufw allow 139/tcp && sudo ufw allow 445/tcp && sudo systemctl enable smb.service && sudo systemctl start smb.service
```
> check smb://your-server_ip/externalHD

&nbsp;

## 24. FreshRSS
```
pikaur -S freshrss --noconfirm && sudo nano /etc/nginx/sites-available/freshrss
```
Add and change your IP address:

```
server {
        listen 7666; # http on port 80

        # your server's url(s)
        server_name 192.168.1.76; # Your server IP address

        # the folder p of your FreshRSS installation
        root /usr/share/webapps/freshrss/p/;

        index index.php index.html index.htm;

        # nginx log files
        access_log /var/log/nginx/rss.access.log;
        error_log /var/log/nginx/rss.error.log;


        # php files handling
        # this regex is mandatory because of the API
        location ~ ^.+?\.php(/.*)?$ {

#               fastcgi_pass 127.0.0.1:9000;
                fastcgi_pass unix:/run/php-fpm/php-fpm.sock;

                fastcgi_split_path_info ^(.+\.php)(/.*)$;

        # By default, the variable PATH_INFO is not set under PHP-FPM
        # But FreshRSS API greader.php need it. If you have a "Bad Request" error, double check this var !
                fastcgi_param PATH_INFO $fastcgi_path_info;
                include fastcgi_params;
                fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        }


        location / {
                try_files $uri $uri/ index.php;
        }

}
```
ctrl + x  
yes

```
sudo ufw allow 7666/tcp && sudo nano /etc/php/php.ini
```
Uncomment:

```
;extension=gmp
```
to

```
extension=gmp
```
ctrl + x  
yes

```
sudo ln -s /etc/nginx/sites-available/freshrss /etc/nginx/sites-enabled/
```
```
sudo systemctl restart php-fpm.service && sudo systemctl restart nginx
```

&nbsp;

__Input required:__    
```
mysql -u root -p
your-password
```

&nbsp;

__Input required:__    
```
CREATE DATABASE FreshRSS;
CREATE USER 'FreshRSS'@'localhost' IDENTIFIED BY 'your-password';
GRANT ALL ON FreshRSS.* TO 'FreshRSS'@'localhost';
FLUSH PRIVILEGES;
exit
```

> Check http://your-server_ip:7666 and follow the instructions.

Database = FreshRSS  
Database USER = FreshRSS  
Password = your-password  

&nbsp;

### Automatic feed update
```
sudo crontab -e
```
Add:

```
#######################FreshRSS Updates
0 */3 * * * php -f /usr/share/webapps/freshrss/app/actualize_script.php > /tmp/FreshRSS.log 2>&1
################################
```

&nbsp;

## 25. FireFox Sync Server
```
sudo pacman -S python2-virtualenv --noconfirm && cd && cd software && git clone https://github.com/mozilla-services/syncserver.git && cd syncserver && make build
```

&nbsp;

__Input required:__    
```
mysql -u root -p
your-password
```

&nbsp;

__Input required:__    
```
CREATE DATABASE ffsync;
CREATE USER 'ffsync'@'localhost' IDENTIFIED BY 'your-password';
GRANT ALL ON ffsync.* TO 'ffsync'@'localhost';
FLUSH PRIVILEGES;
exit
```
```
nano syncserver.ini
```
Add under
```
#sqluri = sqlite:////tmp/syncserver.db
```

&nbsp;

__Input required:__    
```
sqluri = pymysql://ffsync:your-password@localhost:3306/ffsync
```
and change the IP address

```
public_url = http://192.168.1.76:5000/
```
ctrl + x  
yes

```
crontab -e
```
Add:  
> This is only for your pwoss user. Do not be surprised if the others are not listed.

```
#####################ffsync
@reboot sleep 120 && cd /home/pwoss/software/syncserver/ && make serve
##########################################
```
ctrl + x  
yes

```
sudo ufw allow 5000/tcp
```
```
cd && cd /home/pwoss/software/syncserver/ && make serve
```

> Check http://your-server_ip:5000/  
> it work's!  
> Is the answer!

ctrl + c  
To cancel the action.

&nbsp;

### Clients
To configure desktop Firefox to talk to your new Sync server, go to “about:config”, search for “identity.sync.tokenserver.uri” and change its value to the URL of your server with a path of “token/1.0/sync/1.5”:  

- identity.sync.tokenserver.uri: http://sync.example.com/token/1.0/sync/1.5  

Alternatively, if you’re running your own Firefox Accounts server, and running Firefox 52 or later, see the documentation on how to Run your own Firefox Accounts Server for how to configure your client for both Sync and Firefox Accounts with a single preference.  

Since Firefox 33, Firefox for Android has supported custom sync servers. To configure Android Firefox 44 and later to talk to your new Sync server, just set the “identity.sync.tokenserver.uri” exactly as above before signing in to Firefox Accounts and Sync on your Android device.  

Important: after creating the Android account, changes to “identity.sync.tokenserver.uri” will be ignored. (If you need to change the URI, delete the Android account using the Settings > Sync > Disconnect... menu item, update the pref, and sign in again.) Non-default TokenServer URLs are displayed in the Settings > Sync panel in Firefox for Android, so you should be able to verify your URL there.  

Prior to Firefox 44, a custom add-on was needed to configure Firefox for Android. For Firefox 43 and earlier, see the blog post How to connect Firefox for Android to self-hosted Firefox Account and Firefox Sync servers.  

(Prior to Firefox 42, the TokenServer preference name for Firefox Desktop was “services.sync.tokenServerURI”. While the old preference name will work in Firefox 42 and later, the new preference is recommended as the old preference name will be reset when the user signs out from Sync causing potential confusion.)  

&nbsp;

### Updating the server
You should periodically update your code to make sure you’ve got the latest fixes. The following commands will update syncserver in place:  

```  
cd /home/pwoss/software/syncserver  
$ git stash       # to save any local changes to the config file  
$ git pull        # to fetch latest updates from github  
$ git stash pop   # to re-apply any local changes to the config file  
$ make build      # to pull in any updated dependencies  
```

&nbsp;

### Restart ff-sync-server
```
ps aux | grep make
```
Check “make serve” and copy the id (first number)

```
kill (id number)
cd /home/pwoss/software/syncserver/ && make serve
```

&nbsp;

## 25. fail2ban
```
sudo pacman -S fail2ban --noconfirm && sudo systemctl enable fail2ban.service && sudo systemctl start fail2ban.service
```

&nbsp;

## 26. Swap file
```
fallocate -l 512M /swapfile && chmod 600 /swapfile && mkswap /swapfile && swapon /swapfile
```
```
nano /etc/fstab
```
Add to the bottom of the fstab list:

```
/swapfile none swap defaults 0 0
```
ctrl + x  
yes

&nbsp;

## (Optional) - If you want to change the boot text
```
sudo nano /etc/motd
```
```
################################################
Welcome to your PwOSS-Server

     Website: https://pwoss.xyz
       Forum: https://forum.pwoss.xyz
       Guide: https://guideline.pwoss.xyz
################################################
This image is based on Arch Linux | ARM

     Website: http://archlinuxarm.org
       Forum: http://archlinuxarm.org/forum
         IRC: #archlinux-arm on irc.Freenode.net
################################################
```

&nbsp;

## REBOOT
```
sudo reboot now -h
```

&nbsp;
&nbsp;

## Your server are running...

&nbsp;

- Radicale = Contact and Calendar Server ----> http://your-server_ip:5232  
- Seafile = Cloud Server ----> http://your-server_ip:8000  
- WebDav = WebDav Server ----> http://your-server_ip:8080  
- VPN = Virtual Private Network ----> your-dyndns_domain
- Samba = File Server ----> smb://your-server_ip/externalHD  
- FireFox = Sync Bookmarks/History ----> http://your-server_ip:5000/  
- Pi-hole = Advertising blocker ----> http://your-server_ip:987/admin/  
- FreshRSS = RSS Reader ----> http://your-server_ip:7666  

&nbsp;

Now you’re able to save your personal data on your own servers. To keep it safe against a burglar, natural disasters, hardware defects we suggest to set up the same or similar servers with a friend or family member.

&nbsp;

## __ENJOY__
